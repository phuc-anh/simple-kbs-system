var target = Argument("target", "Default");
var solution = GetFiles("./*.sln").First();

Task("Clean")
    .Does(() =>
{
    
});

Task("Restore")
    .IsDependentOn("Clean")
    .Does(() =>
    {
        NuGetRestore(solution);
    });

Task("Build")
    .IsDependentOn("Restore")
    .Does(() =>
    {
        DotNetBuild(solution);
    }); 

Task("Test")
    .IsDependentOn("Build")
    .Does(() =>
    {
        // todo
    });

Task("Default")
  .IsDependentOn("Build")
  .IsDependentOn("Test")
  .Does(() =>
  {
      //todo
  });

RunTarget(target);